from flask import Flask, request, jsonify
import joblib
import traceback
import pandas as pd
import numpy as np
from fungsi import sentimentanalysis
import sys

app = Flask(__name__)

@app.route("/prediksi", methods = ['POST'])
def prediksi_berita():
    if prediktor:
        try:
            json_ = request.json
            query = json_.values()
            berita = prediktor.get_news(query)
            sentimen = prediktor.prediksi([berita])

            return jsonify({'sentimen': sentimen[0]})
        
        except:
            return jsonify({'trace': traceback.format_exc()})
    
    else:
        print('prepare model first.')
        return ('There is no model to use.')

if __name__ == '__main__':
    try:
        port = int(sys.argv[1])
    except:
        port = 5555
    
    prediktor = joblib.load('prediktor.pkl')
    print('model loaded')

    app.run(port = port, debug = True)