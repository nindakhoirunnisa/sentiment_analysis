#import
import numpy as np
import pandas as pd
import requests
from bs4 import BeautifulSoup
import matplotlib.pyplot as plt
import seaborn as sns
import nltk
from nlp_id.stopword import StopWord #untuk stopwords
from nlp_id.lemmatizer import Lemmatizer #untuk lematize
from nlp_id.tokenizer import PhraseTokenizer #untuk tokenizer

from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split, StratifiedKFold, cross_val_score
from sklearn.pipeline import make_pipeline, Pipeline
from sklearn.metrics import make_scorer, accuracy_score, f1_score
from sklearn.metrics import roc_curve, auc
import re
from sklearn import metrics

data = pd.read_csv('news.csv')
data_clean = data.copy()
data_clean['artikel'] = data_clean['artikel'].apply(lambda x: BeautifulSoup(x, 'lxml').text)
nltk.download('punkt')

def tokenize(text):
    tknzr = PhraseTokenizer()
    return tknzr.tokenize(text)

def lemmatize(text):
    lmtz = Lemmatizer()
    return lmtz.lemmatize(text)

def stopword(text):
    stpwrd = StopWord()
    return stpwrd.remove_stopword(text)

space = re.compile('[/(){}\[\]\|@,;]')
symbols = re.compile('[^0-9a-z #+_]')

def clean_text(text):
    text = text.lower()
    text = space.sub(' ', text)
    text = symbols.sub(' ', text)
    text = text.replace('x', '')
    text = ' '.join(word for word in text.split() if word not in data)
    return text

vectorizer = CountVectorizer(tokenizer = tokenize, lowercase = True, binary = False, ngram_range = (1,1))

data_clean['artikel'] = data_clean['artikel'].apply(clean_text)
data_clean['artikel'] = data_clean['artikel'].apply(stopword)
data_clean['artikel'] = data_clean['artikel'].apply(lemmatize)

train, test = train_test_split(data_clean, test_size = 0.2, random_state = 1)
x_train = train['artikel'].values
x_test = test['artikel'].values
y_train = train['sentimen']
y_test = test['sentimen']

kfolds = StratifiedKFold(n_splits = 5, shuffle = True, random_state = 1)

pipeline_nb = make_pipeline(vectorizer, MultinomialNB())

pipeline_nb.get_params().keys()
pipeline_nb.fit(x_train, y_train)

y_pred = pipeline_nb.predict(x_test)
print('\n akurasi: ', metrics.accuracy_score(y_test, y_pred))

import joblib
joblib.dump(pipeline_nb, 'model.pkl')
class sentimentanalysis:
    #def __init__(self, model = pipeline_nb):
        #self.model = model

    def get_news(self, url):
        response = requests.get(url)
        soup = BeautifulSoup(response.text, 'lxml')
        news = soup.find_all('p')
        list_news = []
        for new in news:
            text = new.get_text()
            list_news.append(text)
            berita = ' '.join(list_news)
        return berita
    
    def prediksi(self, teks):
        model = joblib.load('model.pkl')        
        predik = model.predict(teks)
        return predik

prediktor = sentimentanalysis()

joblib.dump(prediktor, 'prediktor.pkl') #punya get_news juga
prediktor = joblib.load('prediktor.pkl')

#dicomment lagi
berita = prediktor.get_news('https://www.republika.co.id/berita/ekonomi/korporasi/18/05/03/p85rke440-pttep-anggarkan-dana-csr-rp-685-miliar-di-indonesia')
print(berita)
sentimen = prediktor.prediksi([berita])
print('sentimen: ', sentimen[0])